## How to install the List Wizard for Curator Tool

1. Download and copy the file "ListComparisonWizard.dll" to the folder "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool\Wizards"
2. Unblock the DLL file: right click the DLL file, select properties and check "Unblock"
3. Run Curator Tool

## Training Videos on YouTube
Go to [https://youtu.be/k2vumWpw1PE](https://youtu.be/k2vumWpw1PE)

## Source code
Go to [https://gitlab.com/CIP-Development/grin-global_client_international](https://gitlab.com/CIP-Development/grin-global_client_international)